import express from "express";
import cors from "cors";
import { AppDataSource } from "./data/data-source";
import * as dotenv from "dotenv";
import { taskRouter } from "./routers/taskRouter";
import { authRouter } from "./routers/authRouter";
dotenv.config();

const app = express();
const port = 3000;

app.use(express.json());

app.use(cors());

app.use("/api", taskRouter);
app.use("/api", authRouter);

AppDataSource.initialize()
  .then(async () => {
    app.listen(port, () => {
      console.log("Server is running on http://localhost:" + port);
    });
    console.log("Data Source has been initialized!");
  })
  .catch((error) => console.log(error));
