import { Taks } from "../Models/Taks.model";
import { AppDataSource } from "../data/data-source";
import { Task as TaskEntity } from "../entity/Task";
import { Request, Response } from "express";

export class TaskController {
  static createTask = async (req: Request, res: Response) => {
    const task: Taks = { ...req.body } as any;
    const taskRepository = AppDataSource.getRepository(TaskEntity);
    let taskSave = new TaskEntity();
    taskSave = { ...task } as any;
    const response = await taskRepository.save(taskSave);

    if (response) {
      return res.status(200).json({ message: "create", data: response });
    } else {
      throw new Error("Failed to create task");
    }
  };

  static listTask = async (req: Request, res: Response) => {
    const taskRepository = AppDataSource.getRepository(TaskEntity);
    const response = await taskRepository.find();

    if (response) {
      return res.status(200).json({ message: "list", data: response });
    } else {
      throw new Error("Failed to create task");
    }
  };

  static updatedTask = async (req: Request, res: Response) => {
    const task: Taks = { ...req.body } as any;
    const { id } = req.params;
    const taskRepository = AppDataSource.getRepository(TaskEntity);
    let taskDb = await taskRepository.findOne({
      where: {
        id: Number(id),
      },
    });
    taskDb.description = task.description;
    taskDb.title = task.title;
    taskDb.status = task.status;
    const resp = await taskRepository.save(taskDb);
    if (resp) {
      return res.status(200).json({ message: "updated", data: taskDb });
    } else {
      throw new Error("Failed to create task");
    }
  };

  static deleteTask = async (req: Request, res: Response) => {
    const { id } = req.params;
    const taskRepository = AppDataSource.getRepository(TaskEntity);
    let taskDb = await taskRepository.findOne({
      where: {
        id: Number(id),
      },
    });

    const response = await taskRepository.remove(taskDb);

    if (response) {
      return res.status(200).json({ message: "delete", data: response });
    } else {
      throw new Error("Failed to create task");
    }
  };
}
