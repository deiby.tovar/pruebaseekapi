import { Request, Response } from "express";
import { encrypt } from "../helpers/helper";

export class AuthController {
  static async login(req: Request, res: Response) {
    try {
      const { user, password } = req.body;
      if (!user || !password) {
        return res
          .status(500)
          .json({ message: "El usuario y contraseña son requeridos" });
      }

      if (user != "ditovar" || password != "testseek") {
        return res.status(404).json({ message: "usuario no encontrado" });
      }

      const dataUser = {
        email: "deiby.tovar1132@gmail.com",
        name: "Deiby Tovar",
      };
      const token = encrypt.generateToken({ id: user.id });

      return res
        .status(200)
        .json({ message: "Login successful", data: { user: dataUser, token } });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ message: "Internal server error" });
    }
  }
}
