export interface Taks {
  id?: number;
  title: string;
  description: string;
  status: StatusType;
}

export enum StatusType {
  toDo = "Por hacer",
  inProgress = "En progreso",
  complete = "completada",
}
