import * as jwt from "jsonwebtoken";
import * as dotenv from "dotenv";

dotenv.config();
const { JWT_SECRET = "" } = process.env;
export class encrypt {
  static generateToken(payload: any) {
    return jwt.sign(payload, JWT_SECRET, { expiresIn: "1d" });
  }
}
