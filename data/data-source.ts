import "reflect-metadata";
import { DataSource } from "typeorm";

export const AppDataSource = new DataSource({
  type: "sqlite",
  database: "./sqlite.db",
  logging: false,
  synchronize: true,
  entities: [__dirname + "/../entity/**/*.ts"],
  migrations: ["../migrations/**/*.ts"],
  subscribers: [],
});
