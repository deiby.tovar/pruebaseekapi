import express, { Request, Response } from "express";
import { AuthController } from "../controllers/AuthController";

const router = express.Router();

router.post("/login", [], AuthController.login);

export { router as authRouter };
