import express, { Request, Response } from "express";
import { TaskController } from "../controllers/TaskController ";
import { authentification } from "../middlewares/auth.middleware";

const router = express.Router();

router.post("/tasks", [authentification], TaskController.createTask);

router.get("/tasks", [authentification], TaskController.listTask);

router.put("/tasks/:id", [authentification], TaskController.updatedTask);

router.delete("/tasks/:id", [authentification], TaskController.deleteTask);

export { router as taskRouter };
